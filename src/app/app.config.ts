import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppConfig {

  config = {
    name: 'RadicalGrowth',
    title: 'RadicalGrowth',
    version: '1.0.0',
    apiUrl: {
      // backendUrl: 'http://localhost:5000/api/v1/',
      backendUrl: 'http://backendcicd-env.eba-wwe2mibm.us-east-2.elasticbeanstalk.com/api/v1/',
    },
  };

  constructor() {
  }

  getConfig(): any {
    return this.config;
  }
}

