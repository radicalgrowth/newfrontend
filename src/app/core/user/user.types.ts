export interface User {
    id: string;
    _id: string;
    name: string;
    email: string;
    password: string;
    avatar?: string;
    profile_picture?: string;
    photo?: string;
    phone_code?: string;
    phoneNumber?: string;
    address?: string;
    age?: string;
    description?: string;
    role: string,
    gender?: string;
    dob?: string;
    timestamp?: string;
    status?: string;
    organization: string;
    city: string;
    country: string;
    application_name: string;
    user_id?: string; 
}
