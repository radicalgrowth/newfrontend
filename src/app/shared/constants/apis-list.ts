'use strict';

import { AppConfig } from "app/app.config";

const apiConfig = new AppConfig();

export const SERVER_URL: string = apiConfig.config.apiUrl.backendUrl;

// Authentication
export const USER_BASE_URL = `${SERVER_URL}users`;
export const LOGIN_API = `${USER_BASE_URL}/login`;
export const FORGOT_PASSWORD = `${USER_BASE_URL}/forgot-password`;

// User

export const GET_ALL_USERS = `${USER_BASE_URL}/all/{id}`;
export const USER_UPDATE = `${USER_BASE_URL}/{id}`;
export const USER_PERMANENLY_DELETE = `${USER_BASE_URL}/permanent-delete/{id}`;

// Workers
// export const WORKER_BASE_URL = `${SERVER_URL}workers`;
// export const WORKER_UPDATE = `${WORKER_BASE_URL}/{id}`;


// Farms
export const FARM_BASE_URL = `${SERVER_URL}farm`;
export const FARM_DETAIL = `${FARM_BASE_URL}/{id}`;
export const FARM_LISTING = `${FARM_BASE_URL}`;
export const GET_ALL_FARMS_BY_USER = `${FARM_BASE_URL}/user-farms/{id}`;

// Fields
export const FIELD_BASE_URL = `${SERVER_URL}field`;
export const FIELD_UPDATE = `${FIELD_BASE_URL}/{id}`;
export const ALL_FORM_FIELDS = `${FIELD_BASE_URL}/farm-fields/{id}`;
export const GET_ALL_FIELDS_BY_USER = `${FIELD_BASE_URL}/user-fields/{id}`;

// Devices
// export const DEVICE_BASE_URL = `${SERVER_URL}devices`;
// export const DEVICE_GET = `${DEVICE_BASE_URL}/{id}`;
// export const DEVICE_GET_DEVICES = `${DEVICE_BASE_URL}/getthingstackdevices`;
// export const DEVICE_GET_FIELDS = `${DEVICE_BASE_URL}/getthingstackfields`;
// export const GET_ALL_DEVICES_IN_FIELD = `${DEVICE_BASE_URL}/field-devices/{id}`;
// export const DEVICE_DATA = `${DEVICE_BASE_URL}/getdatabydeviceid`;

export const DEVICE_BASE_URL = `${SERVER_URL}devices`;
export const DEVICE_GET = `${DEVICE_BASE_URL}/{id}`;
export const DEVICE_GET_BY_USER = `${DEVICE_BASE_URL}/get-user-devices/{id}`;
export const DEVICE_GET_DEVICES = `${DEVICE_BASE_URL}/get-thingstack-devices`;
export const DEVICE_GET_FIELDS = `${DEVICE_BASE_URL}/get-thingstack-fields`;
export const GET_ALL_DEVICES_IN_FIELD = `${DEVICE_BASE_URL}/field-devices/{id}`;
export const DEVICE_DATA = `${DEVICE_BASE_URL}/get-device-data`;

// Alerts
export const ALERT_BASE_URL = `${SERVER_URL}alerts`;
export const ALERT_GET_BY_USER = `${ALERT_BASE_URL}/get-user-alerts/{id}`;
export const ALERT_GET = `${ALERT_BASE_URL}/{id}`;
export const ALERT_SENT = `${ALERT_BASE_URL}/user-sent-alerts/{id}`;


// OpenWeatherMap
export const WEATHER = 'https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&units=metric&appid=dc213be777caf747e1e8b9556fb064f0';
export const CURRENT_WEATHER = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=metric&appid=dc213be777caf747e1e8b9556fb064f0';

// Dashboard
export const DASHBOARD_BASE_URL = `${SERVER_URL}dashboard`;
export const DASHBOARD_STATS = `${DASHBOARD_BASE_URL}/stats/{id}`;
export const EVENTS_BASE_URL = `${DASHBOARD_BASE_URL}/events`;
export const EVENTS_DETAIL = `${EVENTS_BASE_URL}/{id}`;
export const USER_EVENTS_DETAIL = `${EVENTS_BASE_URL}/user-events/{id}`;
