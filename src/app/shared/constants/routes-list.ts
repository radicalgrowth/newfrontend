'use strict';

// ERROR route
export const ERROR = 'error';

// Users routes
export const USERS_LISTING = '/users';
export const USER_ADD = `${USERS_LISTING}/add`;

// Alerts Routes
export const ALERTS_LISTING = '/alerts';
export const FARMS_LISTING = '/farms';

export const DEVICE_LISTING = '/devices';

