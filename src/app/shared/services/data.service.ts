import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) { }

  fetchData({ apiUrl, method = 'POST', contentType = 'application/json', params = new HttpParams(), body = null, responseType = null }: {
    apiUrl: string, method?: string, contentType?: string, params?: HttpParams, body?: any, accept?: any, responseType?: any
  }): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', contentType).append('Accept', '*/*');
    const options: { headers?: HttpHeaders, params?: HttpParams, body?: any, responseType?: 'json' } = { headers, params, body, responseType };
    return this.httpClient.request(method, apiUrl, options).pipe(map((response: any) => {
      return response
    }), shareReplay());
  }

  postFormData(url: string, formData: any) {
    return this.httpClient.post(url, formData);
  }

  patchFormData(url: string, formData: any) {
    return this.httpClient.patch(url, formData);
  }

  getForkedJoinData(currentWeather, historicalWeather) {
    const weather = this.httpClient.get(currentWeather);
    const forecastWeather = this.httpClient.get(historicalWeather);
    return forkJoin([weather, forecastWeather]);
  }
}
