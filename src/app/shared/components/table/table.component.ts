import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Subscription } from 'rxjs/internal/Subscription';
import { map, takeUntil } from 'rxjs/operators';
import { DataService } from 'app/shared/services/data.service';
import { User } from 'app/core/user/user.types';
import { Subject } from 'rxjs';
import { UserService } from 'app/core/user/user.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})


export class TableComponent implements OnInit {

  isLoading: boolean = false;
  searchValue: string;
  tableData: any[];

  @Input() tableColumns: any[] = [];
  private eventsSubscription: Subscription;
  @Input() eventsSub: Observable<any>;
  @Input() requestBody: any = {};
  @Input() endPointConfiguration: { url: string; endpoint: string; method: string; contentType: string } = { url: '', endpoint: '', method: 'POST', contentType: 'application/json' };

  @Output() checkboxCheckedUpdated: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickedOnRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteRow: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedUsersCount: EventEmitter<any> = new EventEmitter<any>();
  @Output() searchQuery: EventEmitter<any> = new EventEmitter<any>();

  public displayedColumns: string[];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private localStorage: LocalStorageService,
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const columnNames = this.tableColumns.map((tableColumn: any) => tableColumn.value);
    // this.displayedColumns = ['select', ...columnNames]; // to have checkbox functionality in table
    this.displayedColumns = [...columnNames];
    this.loadResource();
    this.eventsSubscription = this.eventsSub.subscribe((action) => {
      if (action.origin == 'users') {
        this.searchValue = action.value;
        this.applyFilter(this.searchValue);
      } else if (action.origin == 'deleted') {
        this.loadResource();
        this.selection.clear();
      }
    });
  }


  ngAfterViewInit() {
  }

  applyFilter(value: string) {
    // this.router.navigate([], { relativeTo: this.route.parent, queryParams: { search: value }, queryParamsHandling: 'merge' }); // remove to replace all query params by provided
    this.dataSource.filter = value.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection?.selected?.length;
    const numRows = this.dataSource?.data?.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  getSelected(selectedRows: any) {
    this.selectedUsersCount.emit({ selectedRows: selectedRows });
  }

  searchValueUpdate() {
    this.searchQuery.emit({ search: this.searchValue ? this.searchValue : '' });
  }

  onClickedRow(item: any): void {
    this.clickedOnRow.emit(item);
  }

  deletedRecord(item: any): void {

    this.deleteRow.emit(item);
  }

  loadResource() {
    let url = this.endPointConfiguration.url;
    if (url.includes('api/v1/alerts/get-user-alerts/{id}') || url.includes('api/v1/users/all/{id}')) {
      this.endPointConfiguration.url = this.endPointConfiguration.url.replace('{id}', this.localStorage.get('user')._id);
    }
    this.isLoading = true;
    this.dataService.fetchData({
      apiUrl: this.endPointConfiguration.url,
      method: this.endPointConfiguration.method,
      contentType: this.endPointConfiguration.contentType,
      params: undefined,
      body: null
    }).pipe(
      map(response => {
        return response;
      }),
    ).subscribe((resource: any) => {
      this.isLoading = false;
      if (resource) {
        this.tableData = resource.body.items;
        this.dataSource = new MatTableDataSource(resource.body.items);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.dataSource.filterPredicate = (data: any, filter) => {
          const dataStr = JSON.stringify(data).toLowerCase();
          return dataStr.indexOf(filter) != -1;
        }
        this.route.queryParams.subscribe((param) => {
          if (param.search) {
            this.searchValue = param.search;
            this.applyFilter(this.searchValue);
            this.searchValueUpdate();
          }
          if (param.page) {
            this.paginator.pageIndex = param.page;
          }
        });
      }
    });

  }

  sortTable(sortParameters: Sort) {
    // defining name of data property, to sort by, instead of column name
    sortParameters.active = this.tableColumns.find(column => column.value === sortParameters.active).key;
    const keyName = sortParameters.active;
    if (sortParameters.direction === 'asc') {
      this.tableData = this.tableData.sort((a: any, b: any) => a[keyName]?.localeCompare(b[keyName]));
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else if (sortParameters.direction === 'desc') {
      this.tableData = this.tableData.sort((a: any, b: any) => b[keyName]?.localeCompare(a[keyName]));
      this.dataSource = new MatTableDataSource(this.tableData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      this.loadResource();
    }

  }

  onStatusToggle(checked: boolean, item: any) {
    this.checkboxCheckedUpdated.emit({ checked: checked, item: item });
  }

  ngOnDestroy(): void {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }
  }
}
