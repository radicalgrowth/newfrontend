import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { TableComponent } from './components/table/table.component';
import { DataGetterPipe } from './components/table/table-getter-pipe';

@NgModule({
    declarations: [
        TableComponent,
        DataGetterPipe
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        TableComponent,
        DataGetterPipe
    ]
})
export class SharedModule
{
}
