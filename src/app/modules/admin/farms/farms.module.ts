import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { SharedModule } from 'app/shared/shared.module';
import { FarmFormComponent } from './farm-form/farm-form.component';
import { AddFarmDialog, FarmListComponent } from './farm-list/farm-list.component';
import { FarmsResolverService } from './farms-resolver.service';
import { FarmsRoutingModule } from './farms-routing.module';
import { AgmCoreModule } from '@agm/core';
import { NgApexchartsModule } from 'ng-apexcharts';
@NgModule({
  declarations: [FarmListComponent, FarmFormComponent, AddFarmDialog],
  imports: [
    ReactiveFormsModule,
    FarmsRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    HttpClientModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDwSMtFfqc50Xee09jOKHRD-S7BtTdy1ps',
      libraries: ['places', 'drawing', 'geometry']
    }),
  ],
  providers: [FarmsResolverService, DataService, LocalStorageService]
})

export class FarmsModule { }
