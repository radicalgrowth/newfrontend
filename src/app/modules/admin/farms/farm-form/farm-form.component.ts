import { AgmMap, MapsAPILoader } from '@agm/core';
import { ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { map } from 'rxjs/operators';
import { ChartComponent } from "ng-apexcharts";
import * as routeList from 'app/shared/constants/routes-list';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FuseConfirmationService } from '@fuse/services/confirmation';

declare const google: any;

@Component({
  selector: 'app-farm-form',
  templateUrl: './farm-form.component.html',
  styleUrls: ['./farm-form.component.scss']
})

export class FarmFormComponent implements OnInit {

  polygon: any;
  polygon2: any;
  lat: any = 33.6844;
  lng: any = 73.0479;
  pointList: { lat: number; lng: number }[] = [];
  drawingManager: any;
  drawingManager2: any;
  selectedShape: any;
  selectedShape2: any;
  private mapSubscription$: Subscription; /* The following helps to observe the load cycle of the maps */
  private geoCoder;
  private map: any; /* Used to hold reference to the map for google.maps for zooming */
  @ViewChild('agmMap', { static: false }) agmMap: AgmMap;
  @ViewChild('agmMap2', { static: false }) agmMap2: AgmMap;
  @ViewChild('search', { static: false }) public search: ElementRef;

  activeTab: any;
  isLoading: boolean = false;
  fieldId: any;
  farmId: any;
  user: any;
  public pageIdParam: any = 'id';

  weather: any = {
    current: null,
    forecast: null
  };

  devices: any;
  activeDevices: any;
  selectedDevice: any;
  deviceDetail: any;
  @ViewChild('chart', { static: false }) chart: ChartComponent;
  public chartOptions: Partial<any>;
  public chartOptions2: Partial<any>;
  xAxis = [];
  moist1 = [];
  moist2 = [];
  soilTemp = [];
  times: any[] = [
    { text: 'Today', from: this.setInitialTime(new Date(), 'today'), to: new Date().toISOString() },
    { text: 'Yesterday', from: this.setInitialTime(new Date(), 'yesterdayFrom'), to: this.setInitialTime(new Date(), 'yesterdayTo') },
    { text: 'Last 7 Days', from: this.setInitialTime(new Date(), 'last7From'), to: new Date().toISOString() },
    { text: 'Last 30 Days', from: this.setInitialTime(new Date(), 'last30From'), to: new Date().toISOString() },
  ];

  timeline: number = 3;

  cropTypes: any[] = [
    { value: 'Alfalfa', label: 'Alfalfa' },
    { value: 'Almond', label: 'Almond' },
    { value: 'Apple', label: 'Apple' },
    { value: 'Avocado', label: 'Avocado' },
    { value: 'Banana (1st Season)', label: 'Banana (1st Season)' },
    { value: 'Barley', label: 'Barley' },
    { value: 'Bean', label: 'Bean' },
    { value: 'Bell-Peppers', label: 'Bell Peppers' },
    { value: 'Berry', label: 'Berry' },
    { value: 'Blackberry', label: 'Blackberry' },
    { value: 'Blueberries', label: 'Blueberries' },
    { value: 'Broccoli', label: 'Broccoli' },
    { value: 'Brussel-Sprouts', label: 'Brussel Sprouts' },
    { value: 'Cabbage', label: 'Cabbage' },
    { value: 'Cacao', label: 'Cacao' },
    { value: 'Carrots', label: 'Carrots' },
    { value: 'Cauliflower', label: 'Cauliflower' },
    { value: 'Cherry', label: 'Cherry' },
    { value: 'Citrus', label: 'Citrus' },
    { value: 'Coffee', label: 'Coffee' },
    { value: 'Corn', label: 'Corn' },
    { value: 'Cotton', label: 'Cotton' },
    { value: 'Cucumber-Pickling', label: 'Cucumber Pickling' },
    { value: 'Cucumber-Slicking', label: 'Cucumber Slicking' },
    { value: 'Custard-Apple', label: 'Ustard Apple' },
    { value: 'Date-Palms', label: 'Date Palms' },
    { value: 'Eggplant', label: 'Egg Plant' },
    { value: 'Fig', label: 'Fig' },
    { value: 'Garlic', label: 'Garlic' },
    { value: 'Grapes', label: 'Grapes' },
    { value: 'Grass', label: 'Grass' },
    { value: 'Grass-Pasture', label: 'Grass Pasture' },
    { value: 'Hazelnuts', label: 'Hazelnuts' },
    { value: 'Head-Lettuce', label: 'Head Lettuce' },
    { value: 'Hops', label: 'Hops' },
    { value: 'Kale', label: 'Kale' },
    { value: 'Leafy-Lettuce', label: 'Leafy Lettuce' },
    { value: 'Lemon', label: 'Lemon' },
    { value: 'Lentile', label: 'Lentile' },
    { value: 'Lychee', label: 'Lychee' },
    { value: 'Macadamia', label: 'Macadamia' },
    { value: 'Maize', label: 'Maize' },
    { value: 'Mango', label: 'Mango' },
    { value: 'Melon', label: 'Melon' },
    { value: 'Mint', label: 'Mint' },
    { value: 'Mustard', label: 'Mustard' },
    { value: 'Nectarine', label: 'Nectarine' },
    { value: 'Oat', label: 'Oat' },
    { value: 'Olive', label: 'Olive' },
    { value: 'Onion', label: 'Onion' },
    { value: 'Orange', label: 'Orange' },
    { value: 'Peach', label: 'Peach' },
    { value: 'Peanut', label: 'Peanut' },
    { value: 'Pear', label: 'Pear' },
    { value: 'Peas', label: 'Peas' },
    { value: 'Pecan', label: 'Pecan' },
    { value: 'Pineapple', label: 'Pineapple' },
    { value: 'Pistachios', label: 'Pistachios' },
    { value: 'Plum', label: 'Plum' },
    { value: 'Potato', label: 'Potato' },
    { value: 'Quinoa', label: 'Quinoa' },
    { value: 'Rapeseed-Canola', label: 'Rapeseed Canola' },
    { value: 'Raspberries', label: 'Raspberries' },
    { value: 'Rice', label: 'Rice' },
    { value: 'Sorghum', label: 'Sorghum' },
    { value: 'Soybeans', label: 'Soybeans' },
    { value: 'Strawberry', label: 'Strawberry' },
    { value: 'Sugarbeet', label: 'Sugarbeet' },
    { value: 'Sugarcan', label: 'Sugarcan' },
    { value: 'Sunflower', label: 'Sunflower' },
    { value: 'Sweet-potato', label: 'Sweet potato' },
    { value: 'Teff', label: 'Teff' },
    { value: 'Tomato', label: 'Tomato' },
    { value: 'Turf-Grass', label: 'Turf Grass' },
    { value: 'Walnuts', label: 'Walnuts' },
    { value: 'Watermelon', label: 'Watermelon' },
    { value: 'Wheat', label: 'Wheat' },
    { value: 'WinterWheat', label: 'Winter Wheat' },
  ]

  systemTypes: any[] = [
    // { label: 'Pivot', value: 'pivot' },
    { label: 'Drip', value: 'drip' },
    { label: 'SDI', value: 'sdi' },
    { label: 'Sprinkler', value: 'sprinkler' },
    { label: 'Furrow', value: 'furrow' },
  ];

  details: any = {
    name: null,
    description: null,
    location: null,
    search_location: null,
    soil: { clay: null, sand: null, silt: null, organic_matter: null, ph: null, cec: null, ec: null },
    crop: { crop_type: null, previous_crop: null, planting_date: null, emergence_date: null },
    irrigation: { system_type: null, pivot_shape: null, name: null, flow_rate: null, pivot_length: null, full_run_time: null, actual_area: null, percent_irrigated: null }
  }

  insights: any[] = [];
  configForm: FormGroup;


  constructor(
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private mapsAPILoader: MapsAPILoader,
    public ngZone: NgZone,
    private cdr: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _dataService: DataService,
    private _fuseConfirmationService: FuseConfirmationService,

  ) {
  }

  ngOnInit() {
    if (this.isEditMode() == null) {
      this.activeTab = 3;
    } else {
      this.activeTab = 0;
    }
    this.configForm = this._formBuilder.group({
      title: 'Delete field',
      message: 'Are you sure you want to delete this field? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Delete', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });
  }

  ngAfterViewInit() {

    // if (this.activeTab == 3) {
    this.setupMap();
    if (this.isEditMode() == null) {
      this.farmId = this.route.snapshot.paramMap.get('farmId');
      this.setCurrentPosition();
    } else {
      this.route?.data.subscribe((data: any) => {
        this.details = data?.data?.body;
        this.insights = this.details?.devices.filter((device) => device.moisture_status === 'low');
        this.details.search_location = null;
        this.fieldId = this.details._id;
        this.farmId = this.details.farm_id;
        this.pointList = this.details?.location;
        this.details.crop.crop_id = this.details?.crop?._id;
        delete this.details.crop._id;
        this.details.soil.soil_id = this.details?.soil?._id;
        delete this.details.soil._id;
        let irrigationType = this.details?.irrigation?.system_type + '_id';
        this.details.irrigation[irrigationType] = this.details?.irrigation?._id;
        delete this.details.irrigation._id;
        if (this.activeTab == 0) {
          this.getDevices();
          this.getWeather();
        }
      });
      // }
    }
    this.cdr.detectChanges();
  }



  isEditMode(param: string = this.pageIdParam): string | null {
    return this.route.snapshot.paramMap.get(param);
  }


  onSubmit(activeTab) {
    if (activeTab == 3) {
      this.settingsTabSubmit();
    }


  }

  setInitialTime(date: any, type: string) {
    if (type == 'today') {
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'yesterdayFrom') {
      date.setDate(date.getDate() - 1);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'yesterdayTo') {
      date.setDate(date.getDate() - 1);
      date.setHours(23);
      date.setMinutes(59);
      date.setSeconds(59);
      return date.toISOString();
    } else if (type == 'last7From') {
      date.setDate(date.getDate() - 6);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'last30From') {
      date.setDate(date.getDate() - 29);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    }
  }

  onMapReady(map) {
    this.initDrawingManager(map);
  }

  initDrawingManager(map: any) {
    const self = this;
    const options = {
      drawingControl: true,
      drawingControlOptions: { drawingModes: ['polygon'] },
      polygonOptions: { draggable: true, editable: true },
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
    };

    this.drawingManager = new google.maps.drawing.DrawingManager(options);
    this.drawingManager.setMap(map);

    google.maps.event.addListener(this.drawingManager, 'overlaycomplete', (event) => {

      if (event.type === google.maps.drawing.OverlayType.POLYGON) {
        const paths = event.overlay.getPaths();

        for (let p = 0; p < paths.getLength(); p++) {

          google.maps.event.addListener(paths.getAt(p), 'set_at', () => {
            if (!event.overlay.drag) {
              self.updatePointList(event.overlay.getPath());
            }
          });

          google.maps.event.addListener(paths.getAt(p), 'insert_at', () => {
            self.updatePointList(event.overlay.getPath());
          });

          google.maps.event.addListener(paths.getAt(p), 'remove_at', () => {
            self.updatePointList(event.overlay.getPath());
          });

        }

        self.updatePointList(event.overlay.getPath());
        this.selectedShape = event.overlay;
        this.selectedShape.type = event.type;
      }

      if (event.type !== google.maps.drawing.OverlayType.MARKER) {
        self.drawingManager.setDrawingMode(null); // Switch back to non-drawing mode after drawing a shape.
        self.drawingManager.setOptions({ drawingControl: false }); // To hide drawing controls
      }
    });

    if (this.isEditMode() != null) {

      self.polygon = new google.maps.Polygon({
        paths: this.details.location,
        editable: true,
        draggable: true,
        geodesic: true,
        map: map,
        drawingControl: false,
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
      });
      this.selectedShape = self.polygon;
      this.selectedShape.type = 'polygon';
      let bounds = new google.maps.LatLngBounds();
      this.details.location.forEach((coord) => bounds.extend(coord));
      this.lat = bounds.getCenter().lat();
      this.lng = bounds.getCenter().lng();
      map.setCenter(bounds.getCenter());
      this.drawingManager.setDrawingMode(null); // Switch back to non-drawing mode after drawing a shape.
      this.drawingManager.setOptions({ drawingControl: false });
      google.maps.event.addListener(self.polygon, 'dragend', () => { this.updatePointList(this.polygon.getPath()) });
      const paths = this.polygon.getPaths();
      for (let p = 0; p < paths.getLength(); p++) {
        google.maps.event.addListener(paths.getAt(p), 'set_at', () => { this.updatePointList(this.polygon.getPath()) });
        google.maps.event.addListener(paths.getAt(p), 'insert_at', () => { this.updatePointList(this.polygon.getPath()) });
        google.maps.event.addListener(paths.getAt(p), 'remove_at', () => { this.updatePointList(this.polygon.getPath()) });
      }
    }
  }


  fieldViewMap(map) {
    const self = this;
    self.polygon2 = new google.maps.Polygon({
      paths: this.details?.location,
      editable: false,
      draggable: false,
      geodesic: false,
      map: map,
      drawingControl: false,
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
    });
    this.selectedShape2 = self.polygon2;
    this.selectedShape2.type = 'polygon';
    let bounds = new google.maps.LatLngBounds();
    this.details?.location?.forEach((coord) => bounds.extend(coord));
    this.lat = bounds.getCenter().lat();
    this.lng = bounds.getCenter().lng();
    map.setCenter(bounds.getCenter());
  }

  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude
      });
    }
  }

  deleteSelectedShape() {
    if (this.selectedShape) {
      this.selectedShape.setMap(null);
      this.pointList = [];
      this.details.location = [];
      this.drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
      this.drawingManager.setOptions({ drawingControl: true }); // To show drawing controls
    }
  }

  updatePointList(path) {
    this.pointList = [];
    const len = path.getLength();
    for (let i = 0; i < len; i++) {
      this.pointList.push(path.getAt(i).toJSON());
    }
    this.details.location = this.pointList;
  }

  private setupMap(): void {
    this.mapSubscription$ = this.agmMap.mapReady.subscribe((map) => {
      this.map = map;
      this.setupGooglePlacesAutoComplete();
    });
  }

  private setupGooglePlacesAutoComplete(): void {
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      const autocomplete = new google.maps.places.Autocomplete(this.search.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace(); //get the place result
          if (place.geometry === undefined || place.geometry === null) { // Verify result
            return;
          }
          // Set latitude & longitude
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        });
      });
    });
  }

  tabChange(event: any) {
    this.details.search_location = null;
    this.cdr.detectChanges();

    if (this.activeTab == 3) {
      this.setupMap();
      if (this.isEditMode() == null) {
        this.setCurrentPosition();
      }
    } else {
      if (this.activeTab == 0) {
        this.getDevices();
        this.getWeather();
      } else if (this.activeTab == 1) {
        this.getDevices();
      } else if (this.activeTab == 2) {

      }
      if (this.mapSubscription$) {
        this.mapSubscription$.unsubscribe();
      }
    }
  }

  getWeather() {
    this.isLoading = true;
    this._dataService.getForkedJoinData(
      apiList.CURRENT_WEATHER.replace('{lat}', this.lat).replace('{lon}', this.lng),
      apiList.WEATHER.replace('{lat}', this.lat).replace('{lon}', this.lng).replace('{exclude}', 'hourly,minutely')
    ).subscribe((res: any) => {
      this.isLoading = false;
      this.weather.current = res[0];
      this.weather.forecast = res[1];
    }, (error: any) => {
      this.isLoading = false;
      console.error(error);
    });
  }

  deviceSelected(value) {
    this.deviceDetail = null;
    this.xAxis = [];
    this.moist1 = [];
    this.moist2 = [];
    this.soilTemp = [];
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_GET.replace('{id}', value),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.deviceDetail = res?.body;
        let field = '';
        this.deviceDetail.fields.forEach(elem => {
          if (elem.name == 'uplink_message_decoded_payload_gndmoist1') {
            field += 'uplink_message_decoded_payload_converted_gndmoist1,';
          } else if (elem.name == 'uplink_message_decoded_payload_gndmoist2') {
            field += 'uplink_message_decoded_payload_converted_gndmoist2,';
          } else {
            field += elem.name + ',';
          }
        });
        field = field.substring(0, field.length - 1);
        this.deviceDetail['field_data'] = field;
        this.getChartDetails();
      }
    });
  }

  getChartDetails() {
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_DATA,
      method: 'POST',
      contentType: 'application/json',
      params: undefined,
      body: { device_topic: this.deviceDetail.device_topic, field: this.deviceDetail.field_data, from: this.times[this.timeline].from, to: this.times[this.timeline].to }
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        res?.body?.items.forEach((elem) => {
          this.xAxis.push(elem.time);
          this.moist1.push(elem?.uplink_message_decoded_payload_converted_gndmoist1);
          this.moist2.push(elem?.uplink_message_decoded_payload_converted_gndmoist2);
          this.soilTemp.push(elem?.uplink_message_decoded_payload_soiltemp);
        });
        this.isLoading = false;
        this.chartOptions = {
          series: [
            { name: "Ground Moisture Level 1", data: this.moist1 },
            { name: "Ground Moisture Level 2", data: this.moist2 },
          ],
          chart: {
            height: 400,
            type: "line",
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                enabled: true,
                delay: 150
              },
              dynamicAnimation: {
                enabled: true,
                speed: 350
              }
            }
          },
          dataLabels: { enabled: false },
          stroke: { width: 3, curve: "smooth", dashArray: [0, 0] },
          title: { text: "Volumetric Water Content ( VWC % )", align: "center" },
          legend: {
            tooltipHoverFormatter: function (val, opts) {
              return (val + " - <strong>" + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + "</strong>");
            }
          },
          markers: { size: 0, hover: { sizeOffset: 6 } },
          xaxis: {
            type: 'datetime',
            labels: { trim: false },
            categories: this.xAxis
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + " %";
              }
            },
          },
          tooltip: {
            x: { format: 'dd/MM/yy HH:mm:ss' },
            y: [
              { title: { formatter: function (val) { return val + " ( % )" } } },
              // { title: { formatter: function (val) { return val + " ( °C )" } } },
              { title: { formatter: function (val) { return val + " ( % )" } } },
            ],
          },
          annotations: {
            position: 'back',
            yaxis: [
              {
                y: 0,
                y2: 40,
                borderColor: '#000',
                // fillColor: '#ffcccb',
                fillColor: '#FF6347',
                label: {
                  borderColor: "#FF4560",
                  style: {
                    color: "#fff",
                    background: "#FF4560"
                  },
                  text: 'Low'
                }
              }
            ],
          },
          grid: { borderColor: "#f1f1f1" }
        };
        this.chartOptions2 = {
          series: [
            { name: "Soil Temperature", data: this.soilTemp },
          ],
          chart: {
            height: 400,
            type: "line",
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                enabled: true,
                delay: 150
              },
              dynamicAnimation: {
                enabled: true,
                speed: 350
              }
            }
          },
          dataLabels: { enabled: false },
          stroke: { width: 3, curve: "smooth", dashArray: [0, 0] },
          title: { text: "Soil Temperature ( °C )", align: "center" },
          legend: {
            tooltipHoverFormatter: function (val, opts) {
              return (val + " - <strong>" + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + "</strong>");
            }
          },
          markers: { size: 0, hover: { sizeOffset: 6 } },
          xaxis: {
            type: 'datetime',
            labels: { trim: false },
            categories: this.xAxis
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + " °C";
              }
            },
          },
          tooltip: {
            x: { format: 'dd/MM/yy HH:mm:ss' },
            y: [
              { title: { formatter: function (val) { return val + " ( °C )" } } },
            ],
          },
          grid: { borderColor: "#f1f1f1" }
        };
      } else {
        this.isLoading = false;
        this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
      }
    });

  }

  getDevices() {
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.GET_ALL_DEVICES_IN_FIELD.replace('{id}', this.fieldId),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.devices = res?.body?.items;
        this.activeDevices = this.devices.filter((device) => device.status == 'active');
      }
    });
  }

  settingsTabSubmit() {
    this.details.crop.planting_date = new Date(this.details.crop.planting_date).toISOString();
    this.details.crop.emergence_date = new Date(this.details.crop.emergence_date).toISOString();
    this.details.farm_id = this.farmId;
    this.isLoading = true;
    if (this.isEditMode() == null) {
      this._dataService.fetchData({
        apiUrl: apiList.FIELD_BASE_URL,
        method: 'POST',
        contentType: 'application/json',
        params: undefined,
        body: this.details
      }).pipe(map(response => { return response }),
      ).subscribe((res: any) => {
        this.isLoading = false;
        if (res.status == 'Success') {
          this._snackBar.open('Field settings added succesfully.', 'Added', { duration: 2000 });
        } else {
          this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
        }
      });
    } else {
      this._dataService.fetchData({
        apiUrl: apiList.FIELD_UPDATE.replace('{id}', this.fieldId),
        method: 'PATCH',
        contentType: 'application/json',
        params: undefined,
        body: this.details
      }).pipe(map(response => { return response }),
      ).subscribe((res: any) => {
        this.isLoading = false;
        if (res.status == 'Success') {
          this._snackBar.open('Field settings updated succesfully.', 'Updated', { duration: 2000 });
        } else {
          this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
        }
      });
    }
  }

  deleteField() {
    const dialogRef = this._fuseConfirmationService.open(this.configForm.value);
    dialogRef.afterClosed().subscribe((result) => { // Subscribe to afterClosed from the dialog reference
      if (result == 'confirmed') {
        this.isLoading = true;
        this._dataService.fetchData({
          apiUrl: apiList.FIELD_UPDATE.replace('{id}', this.fieldId),
          method: 'DELETE',
          contentType: 'application/json',
          params: undefined,
          body: null
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status === 'Success') {
            this._snackBar.open('Field is Deleted', 'Deleted', { duration: 2000 });
            setTimeout(() => { this.router.navigateByUrl(routeList.FARMS_LISTING) }, 1000);
          } else {
            this._snackBar.open(res?.message, 'Failed', { duration: 2000 });
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.mapSubscription$) {
      this.mapSubscription$.unsubscribe();
    }
  }



}

