import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FarmListComponent } from './farm-list/farm-list.component';
import { FarmFormComponent } from './farm-form/farm-form.component';
import { FarmsResolverService } from './farms-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: FarmListComponent,
  },
  {
    path: 'add/:farmId',
    component: FarmFormComponent,
  },
  {
    path: ':id',
    component: FarmFormComponent,
    resolve: {
      data: FarmsResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class FarmsRoutingModule { }
