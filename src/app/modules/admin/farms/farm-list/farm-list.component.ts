import { Component, Inject, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { map, Subject, takeUntil } from 'rxjs';
@Component({
  selector: 'app-farm-list',
  templateUrl: './farm-list.component.html',
  styleUrls: ['./farm-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class FarmListComponent implements OnInit, OnDestroy {

  user: User;
  configForm: FormGroup;
  drawerMode: 'over' | 'side' = 'side';
  drawerOpened: boolean = true;
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  isLoading: boolean = false;

  farms: any[] = [];
  fields: any[] = [];
  activeFarm: any;
  apiCalled: boolean = false;

  constructor(
    private _userService: UserService,
    private _fuseMediaWatcherService: FuseMediaWatcherService,
    private _dataService: DataService,
    public _matDialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _fuseConfirmationService: FuseConfirmationService,
    private _formBuilder: FormBuilder,
  ) {

  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------


  ngOnInit(): void {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
    });
    this.getAllFarms(); // Get All Farms

    // Subscribe to media changes
    this._fuseMediaWatcherService.onMediaChange$.pipe(takeUntil(this._unsubscribeAll)).subscribe(({ matchingAliases }) => {
      // Set the drawerMode and drawerOpened
      if (matchingAliases.includes('lg')) {
        this.drawerMode = 'side';
        this.drawerOpened = true;
      }
      else {
        this.drawerMode = 'over';
        this.drawerOpened = false;
      }
    });
    this.configForm = this._formBuilder.group({
      title: 'Delete farm',
      message: 'Are you sure you want to delete this farm? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Delete', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

  getAllFarms() {
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.GET_ALL_FARMS_BY_USER.replace('{id}', this.user._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.farms = res?.body?.items;
        // if (this.farms.length) {
        //   this.openFieldDetails(this.farms[this.farms.length - 1]);
        // }
      }
    });
  }

  openDialog() {
    const dialogRef = this._matDialog.open(AddFarmDialog, { autoFocus: false, data: { userId: this.user._id } });
    dialogRef.afterClosed().subscribe(res => {
      if (res?.event == 'added') {
        this.farms.push(res?.data);
      }
    });
  }

  openFieldDetails(farm: any) {
    this.activeFarm = farm;
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.ALL_FORM_FIELDS.replace('{id}', farm._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      this.apiCalled = true;
      if (res.status == 'Success') {
        this.fields = res?.body?.items;
      }
    });
  }

  addFieldDetails(type: string, data: any = null) {
    if (type == 'edit') {
      this.router.navigate(['./', data._id], { relativeTo: this.route });
    } else {
      this.router.navigate(['./add', this.activeFarm._id], { relativeTo: this.route });
    }
  }

  deleteFarm() {
    const dialogRef = this._fuseConfirmationService.open(this.configForm.value);
    dialogRef.afterClosed().subscribe((result) => { // Subscribe to afterClosed from the dialog reference
      if (result == 'confirmed') {
        this.isLoading = true;
        this._dataService.fetchData({
          apiUrl: apiList.FARM_DETAIL.replace('{id}', this.activeFarm._id),
          method: 'DELETE',
          contentType: 'application/json',
          params: undefined,
          body: null
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status === 'Success') {
            this._snackBar.open('Farm is Deleted', 'Deleted', { duration: 2000 });
            this.farms = this.farms.filter((farm) => farm._id != this.activeFarm._id);
            this.activeFarm = null;
          } else {
            this._snackBar.open(res?.message, 'Failed', { duration: 2000 });
          }
        });
      }
    });
  }

}


@Component({
  selector: 'add-farm-dialog',
  templateUrl: 'add-farm-dialog.html',
})

export class AddFarmDialog implements OnInit {

  farmName: string;

  constructor(
    public dialogRef: MatDialogRef<AddFarmDialog>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  save() {

    let body = { name: this.farmName, user_id: this._data.userId }
    this._dataService.fetchData({
      apiUrl: apiList.FARM_BASE_URL,
      method: 'POST',
      contentType: 'application/json',
      params: undefined,
      body: body
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        this._snackBar.open('Farm added succesfully.', 'Added', { duration: 2000 });
        this.dialogRef.close({ event: 'added', data: res?.body });
      }
    });
  }
}