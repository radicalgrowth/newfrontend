import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddDeviceResolverService } from './add-device-resolver.service';
import { AddDeviceFormComponent } from './add-device/add-device-form.component';
import { DeviceChartsComponent } from './device-charts/device-charts.component';
import { DevicesListComponent } from './devices-list/devices-list.component';
import { DevicesResolverService } from './devices-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: DevicesListComponent,
  },
  {
    path: ':id',
    component: DeviceChartsComponent,
    resolve: {
      data: DevicesResolverService
    }
  },
  {
    path: 'add/:id',
    component: AddDeviceFormComponent,
    resolve: {
      data: AddDeviceResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DevicesRoutingModule { }
