import { AgmMap } from '@agm/core';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import * as routeList from 'app/shared/constants/routes-list';
@Component({
  selector: 'app-add-device-form',
  templateUrl: './add-device-form.component.html',
  styleUrls: ['./add-device-form.component.scss']
})

export class AddDeviceFormComponent implements OnInit {

  lat: any = 33.6844;
  lng: any = 73.0479;
  drawingManager: any;
  polygon: any;
  selectedShape: any;
  private mapSubscription$: Subscription; /* The following helps to observe the load cycle of the maps */
  private geoCoder;
  public map: any; /* Used to hold reference to the map for google.maps for zooming */
  @ViewChild('agmMap', { static: false }) agmMap: AgmMap;
  markers: any = [];
  markerAdded: boolean = false;

  user: User;
  isLoading: boolean = false;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  deviceType: string;
  fields: any;
  devices: any;
  deviceFields: any;
  deviceSelectedIndex: number = null;

  details: any;

  constructor(
    private _userService: UserService,
    public ngZone: NgZone,
    private router: Router,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.setCurrentPosition();
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
      this.getFields();
      this.getDevices();
    });
    this.route?.data.subscribe((data: any) => {
      this.details = data?.data?.body;
    });
  }

  ngAfterViewInit() {
    this.setupMap();
  }

  private setupMap(): void {
    this.mapSubscription$ = this.agmMap.mapReady.subscribe((map) => {
      this.map = map;
    });
  }



  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude
      });
    }
  }

  markerDragEnd(event: any, i: number): void {
    this.lat = event.latLng.lat();
    this.lng = event.latLng.lng();
    this.markers[0].lat = event.latLng.lat();
    this.markers[0].lng = event.latLng.lng();
  }


  fieldViewMap(map) {
    setTimeout(() => {
      const self = this;
      self.polygon = new google.maps.Polygon({
        paths: this.details?.location,
        editable: false,
        draggable: false,
        geodesic: false,
        map: map,
      });
      this.selectedShape = self.polygon;
      this.selectedShape.type = 'polygon';
      let bounds = new google.maps.LatLngBounds();
      this.details?.location?.forEach((coord) => bounds.extend(coord));
      this.lat = bounds.getCenter().lat();
      this.lng = bounds.getCenter().lng();
      map.setCenter(bounds.getCenter());

    }, 1500);

  }

  submit() {
    if (this.deviceSelectedIndex != null && this.deviceType && this.deviceFields.length && this.markers.length) {
      let body = {
        field_id: this.details._id,
        device_type: this.deviceType,
        fields: this.deviceFields,
        name: this.devices[this.deviceSelectedIndex].label,
        application_name: this.devices[this.deviceSelectedIndex].application_name,
        device_topic: this.devices[this.deviceSelectedIndex].value,
        device_location: {lat: this.markers[0].lat, long: this.markers[0].lng}
      }
      this._dataService.fetchData({
        apiUrl: apiList.DEVICE_BASE_URL,
        method: 'POST',
        contentType: 'application/json',
        params: undefined,
        body: body
      }).pipe(map(response => { return response }),
      ).subscribe((res: any) => {
        if (res.status == 'Success') {
          this._snackBar.open('Device added succesfully.', 'Added', { duration: 2000 });
          setTimeout(() => { this.router.navigateByUrl(routeList.DEVICE_LISTING) }, 1000);
        } else {
          this._snackBar.open(res?.message ? res?.message : 'There is some error', 'Failed', { duration: 2000 });
        }
      });
    } else {
      this._snackBar.open('Fill all the inputs and place a device marker.', 'Failed', { duration: 2000 });
    }

  }

  getFields() {
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_GET_FIELDS,
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        this.fields = res?.body?.items;
      }
    });
  }

  getDevices() {
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_GET_DEVICES,
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        this.devices = res?.body?.items;
        this.devices = this.devices.filter((device)=> device?.value.includes(this.user?.email));
      }
    });
  }

  addDeviceLocation() {
    this.markerAdded = true;
    this.markers.push({
      label: 'Device Pin',
      lat: this.lat,
      lng: this.lng,
      icon: 'pin',
    });

  }

  removeDeviceLocationMarker() {
    this.markerAdded = false;
    this.markers = [];
  }

  ngOnDestroy(): void {
    if (this.mapSubscription$) {
      this.mapSubscription$.unsubscribe();
    }
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}

