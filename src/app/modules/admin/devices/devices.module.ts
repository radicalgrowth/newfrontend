import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { SharedModule } from 'app/shared/shared.module';
import { NgApexchartsModule } from 'ng-apexcharts';
import { AddDeviceResolverService } from './add-device-resolver.service';
import { AddDeviceFormComponent } from './add-device/add-device-form.component';
import { DeviceChartsComponent } from './device-charts/device-charts.component';
import { DevicesListComponent } from './devices-list/devices-list.component';
import { DevicesResolverService } from './devices-resolver.service';
import { DevicesRoutingModule } from './devices-routing.module';

@NgModule({
  declarations: [DevicesListComponent, DeviceChartsComponent, AddDeviceFormComponent],
  imports: [
    ReactiveFormsModule,
    DevicesRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    NgApexchartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDwSMtFfqc50Xee09jOKHRD-S7BtTdy1ps',
      libraries: ['places', 'drawing', 'geometry']
    }),
  ],
  providers: [DataService, LocalStorageService, DevicesResolverService, AddDeviceResolverService]
})

export class DevicesModule { }
