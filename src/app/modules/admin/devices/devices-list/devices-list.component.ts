import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseMediaWatcherService } from '@fuse/services/media-watcher';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { map, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-devices-list',
  templateUrl: './devices-list.component.html',
  styleUrls: ['./devices-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DevicesListComponent implements OnInit, OnDestroy {

  user: User;
  drawerMode: 'over' | 'side' = 'side';
  drawerOpened: boolean = true;
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  isLoading: boolean = false;

  fields: any[] = [];
  devices: any[] = [];
  activeField: any;
  apiCalled: boolean = false;

  constructor(
    private _userService: UserService,
    private _fuseMediaWatcherService: FuseMediaWatcherService,
    private _dataService: DataService,
    public _matDialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
  ) {

  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------


  ngOnInit(): void {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
    });
    this.getAllFields(); // Get All Farms

    // Subscribe to media changes
    this._fuseMediaWatcherService.onMediaChange$.pipe(takeUntil(this._unsubscribeAll)).subscribe(({ matchingAliases }) => {
      // Set the drawerMode and drawerOpened
      if (matchingAliases.includes('lg')) {
        this.drawerMode = 'side';
        this.drawerOpened = true;
      }
      else {
        this.drawerMode = 'over';
        this.drawerOpened = false;
      }
    });
  }



  getAllFields() {
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.GET_ALL_FIELDS_BY_USER.replace('{id}', this.user._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.fields = res?.body?.items;
        // if (this.fields.length) {
        //   this.openFieldDetails(this.fields[this.fields.length - 1]);
        // }
      }
    });
  }

  openFieldDetails(field: any) {
    this.activeField = field;
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.GET_ALL_DEVICES_IN_FIELD.replace('{id}', field._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      this.apiCalled = true;
      if (res.status == 'Success') {
        this.devices = res?.body?.items;
      }
    });
  }

  viewDetails(data: any = null) {
    this.router.navigate(['./', data._id], { relativeTo: this.route });
  }

  addDevice() {
    this.router.navigate(['./add', this.activeField._id], { relativeTo: this.route });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
