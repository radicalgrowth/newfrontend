import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { ChartComponent } from "ng-apexcharts";
import { map, Subject, takeUntil } from 'rxjs';
import * as routeList from 'app/shared/constants/routes-list';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { FormBuilder, FormGroup } from '@angular/forms';

// export type ChartOptions = { series: ApexAxisChartSeries; chart: ApexChart; xaxis: ApexXAxis; stroke: ApexStroke; dataLabels: ApexDataLabels; markers: ApexMarkers; tooltip: any; yaxis: ApexYAxis; grid: ApexGrid; legend: ApexLegend; title: ApexTitleSubtitle };
@Component({
  selector: 'app-device-charts',
  templateUrl: './device-charts.component.html',
  styleUrls: ['./device-charts.component.scss']
})

export class DeviceChartsComponent implements OnInit {

  user: User;
  configForm: FormGroup;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<any>;
  public chartOptions2: Partial<any>;

  xAxis = [];
  moist1 = [];
  moist2 = [];
  soilTemp = [];

  details: any;
  isLoading: boolean = false;
  public pageIdParam: any = 'id';

  times: any[] = [
    { text: 'Today', from: this.setInitialTime(new Date(), 'today'), to: new Date().toISOString() },
    { text: 'Yesterday', from: this.setInitialTime(new Date(), 'yesterdayFrom'), to: this.setInitialTime(new Date(), 'yesterdayTo') },
    { text: 'Last 7 Days', from: this.setInitialTime(new Date(), 'last7From'), to: new Date().toISOString() },
    { text: 'Last 30 Days', from: this.setInitialTime(new Date(), 'last30From'), to: new Date().toISOString() },
  ];

  timeline: number = 3;
  constructor(
    private _snackBar: MatSnackBar,
    private _userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    public _matDialog: MatDialog,
    private _dataService: DataService,
    private _fuseConfirmationService: FuseConfirmationService,
    private _formBuilder: FormBuilder,
  ) {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
    });
  }

  setInitialTime(date: any, type: string) {
    if (type == 'today') {
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'yesterdayFrom') {
      date.setDate(date.getDate() - 1);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'yesterdayTo') {
      date.setDate(date.getDate() - 1);
      date.setHours(23);
      date.setMinutes(59);
      date.setSeconds(59);
      return date.toISOString();
    } else if (type == 'last7From') {
      date.setDate(date.getDate() - 6);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    } else if (type == 'last30From') {
      date.setDate(date.getDate() - 29);
      date.setUTCHours(0, 0, 0, 0);
      return date.toISOString();
    }
  }

  ngOnInit() {
    if (this.isEditMode() == null) {
    } else {
      this.route?.data.subscribe((data: any) => {
        this.details = data?.data?.body;
        let field = '';
        this.details.fields.forEach(elem => {
          if (elem.name == 'uplink_message_decoded_payload_gndmoist1') {
            field += 'uplink_message_decoded_payload_converted_gndmoist1,';
          } else if (elem.name == 'uplink_message_decoded_payload_gndmoist2') {
            field += 'uplink_message_decoded_payload_converted_gndmoist2,';
          } else {
            field += elem.name + ',';
          }
        });
        field = field.substring(0, field.length - 1);
        this.details['field_data'] = field;
        this.getChartDetails();
      });

    }
    this.configForm = this._formBuilder.group({
      title: 'Archive device',
      message: 'Are you sure you want to archive this device? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Archive', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });
  }

  isEditMode(param: string = this.pageIdParam): string | null {
    return this.route.snapshot.paramMap.get(param);
  }


  onSubmit() {
  }

  openDialog() {
  }

  getChartDetails() {
    this.isLoading = true;
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_DATA,
      method: 'POST',
      contentType: 'application/json',
      params: undefined,
      body: { device_topic: this.details.device_topic, field: this.details.field_data, from: this.times[this.timeline].from, to: this.times[this.timeline].to }
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        res?.body?.items.forEach((elem) => {
          this.xAxis.push(elem.time);
          this.moist1.push(elem?.uplink_message_decoded_payload_converted_gndmoist1);
          this.moist2.push(elem?.uplink_message_decoded_payload_converted_gndmoist2);
          this.soilTemp.push(elem?.uplink_message_decoded_payload_soiltemp);
        });
        this.isLoading = false;
        this.chartOptions = {
          series: [
            { name: "Ground Moisture Level 1", data: this.moist1 },
            { name: "Ground Moisture Level 2", data: this.moist2 },
          ],
          chart: {
            height: 400,
            type: "line",
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                enabled: true,
                delay: 150
              },
              dynamicAnimation: {
                enabled: true,
                speed: 350
              }
            }
          },
          dataLabels: { enabled: false },
          stroke: { width: 3, curve: "smooth", dashArray: [0, 0] },
          title: { text: "Volumetric Water Content ( VWC % )", align: "center" },
          legend: {
            tooltipHoverFormatter: function (val, opts) {
              return (val + " - <strong>" + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + "</strong>");
            }
          },
          markers: { size: 0, hover: { sizeOffset: 6 } },
          xaxis: {
            type: 'datetime',
            labels: { trim: false },
            categories: this.xAxis
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + " %";
              }
            },
          },
          tooltip: {
            x: { format: 'dd/MM/yy HH:mm:ss' },
            y: [
              { title: { formatter: function (val) { return val + " ( % )" } } },
              // { title: { formatter: function (val) { return val + " ( °C )" } } },
              { title: { formatter: function (val) { return val + " ( % )" } } },
            ],
          },
          annotations: {
            position: 'back',
            yaxis: [
              {
                y: 0,
                y2: 40,
                borderColor: '#000',
                // fillColor: '#ffcccb',
                fillColor: '#FF6347',
                label: {
                  borderColor: "#FF4560",
                  style: {
                    color: "#fff",
                    background: "#FF4560"
                  },
                  text: 'Low'
                }
              }
            ],
          },
          grid: { borderColor: "#f1f1f1" }
        };
        this.chartOptions2 = {
          series: [
            { name: "Soil Temperature", data: this.soilTemp },
          ],
          chart: {
            height: 400,
            type: "line",
            animations: {
              enabled: true,
              easing: 'easeinout',
              speed: 800,
              animateGradually: {
                enabled: true,
                delay: 150
              },
              dynamicAnimation: {
                enabled: true,
                speed: 350
              }
            }
          },
          dataLabels: { enabled: false },
          stroke: { width: 3, curve: "smooth", dashArray: [0, 0] },
          title: { text: "Soil Temperature ( °C )", align: "center" },
          legend: {
            tooltipHoverFormatter: function (val, opts) {
              return (val + " - <strong>" + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + "</strong>");
            }
          },
          markers: { size: 0, hover: { sizeOffset: 6 } },
          xaxis: {
            type: 'datetime',
            labels: { trim: false },
            categories: this.xAxis
          },
          yaxis: {
            labels: {
              formatter: function (value) {
                return value + " °C";
              }
            },
          },
          tooltip: {
            x: { format: 'dd/MM/yy HH:mm:ss' },
            y: [
              { title: { formatter: function (val) { return val + " ( °C )" } } },
            ],
          },
          grid: { borderColor: "#f1f1f1" }
        };
      } else {
        this.isLoading = false;
        this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
      }
    });

  }

  timelineChange(value) {
    this.xAxis = [];
    this.moist1 = [];
    this.moist2 = [];
    this.soilTemp = [];
    this.getChartDetails();
  }

  archiveDevice() {
    const dialogRef = this._fuseConfirmationService.open(this.configForm.value);
    dialogRef.afterClosed().subscribe((result) => { // Subscribe to afterClosed from the dialog reference
      if (result == 'confirmed') {
        this.isLoading = true;
        this._dataService.fetchData({
          apiUrl: apiList.DEVICE_GET.replace('{id}', this.details?._id),
          method: 'DELETE',
          contentType: 'application/json',
          params: undefined,
          body: null
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('Device archived succesfully.', 'Archived', { duration: 2000 });
            this.router.navigateByUrl(routeList.DEVICE_LISTING)
          } else {
            this._snackBar.open('There is some problem', 'Failed', { duration: 2000 });
          }
        });
      }
    });



  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
