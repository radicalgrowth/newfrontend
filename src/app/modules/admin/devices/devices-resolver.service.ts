import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { DataService } from 'app/shared/services/data.service';
import * as apiList from 'app/shared/constants/apis-list';
import { Observable } from 'rxjs/internal/Observable';
import { take } from 'rxjs';

@Injectable()
export class DevicesResolverService implements Resolve<any> {

  constructor(public dataService: DataService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<any> | Promise<any> | any {
    const id = route.paramMap.get('id');
    return this.dataService.fetchData({
      apiUrl: apiList.DEVICE_GET.replace('{id}', id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(take(1));
  }
}
