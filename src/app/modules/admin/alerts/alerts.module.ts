import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { SharedModule } from 'app/shared/shared.module';
import { AlertFormComponent } from './alert-form/alert-form.component';
import { AlertListComponent } from './alert-list/alert-list.component';
import { AlertsResolverService } from './alerts-resolver.service';
import { AlertsRoutingModule } from './alerts-routing.module';
// import { UsersComponent } from './users.component';

@NgModule({
  declarations: [AlertListComponent, AlertFormComponent],
  imports: [
    ReactiveFormsModule,
    AlertsRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [AlertsResolverService, DataService, LocalStorageService]
})

export class AlertsModule { }
