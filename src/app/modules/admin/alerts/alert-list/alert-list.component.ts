import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/shared/services/data.service';
import { fromEvent, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith, takeUntil, tap } from 'rxjs/operators';
import * as apiList from 'app/shared/constants/apis-list';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'app/core/user/user.types';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'app-alert-list',
  templateUrl: './alert-list.component.html',
  styleUrls: ['./alert-list.component.scss']
})

export class AlertListComponent {

  private _unsubscribeAll: Subject<any> = new Subject<any>();
  user: User;
  isLoading: boolean = false;
  configForm: FormGroup;
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  searchControl = new FormControl();
  selectedRecords: any[] = [];
  eventsSubject: Subject<any> = new Subject<any>();
  endPointConfiguration: any = { url: apiList.ALERT_GET_BY_USER, method: 'GET' };
  tableColumns: any;
  requestBody: any;
  searchValue: string;

  @ViewChild('input') input: ElementRef;

  constructor(
    private _userService: UserService,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _fuseConfirmationService: FuseConfirmationService,
  ) {
    this.setTableConfigurationByLanguage();
  }

  ngOnInit() {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
    });

    this.filteredOptions = this.searchControl.valueChanges.pipe(startWith(''), map((value: string) => this._filter(value)));
    this.configForm = this._formBuilder.group({
      title: 'Remove alert',
      message: 'Are you sure you want to remove this alert? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Remove', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });
  }

  ngAfterViewInit() {

    fromEvent(this.input.nativeElement, 'keyup').pipe(filter(Boolean), debounceTime(500), distinctUntilChanged(), tap(() => {
      this.searchValue = this.input.nativeElement.value;
      this.eventsSubject.next({ origin: 'users', value: this.searchValue });
    })).subscribe();

  }

  setTableConfigurationByLanguage(tran?: any): void {
    this.tableColumns = [
      { key: 'alert_name', value: 'Name', type: 'text', class: 'capitalize', isSortable: true, },
      { key: 'device_name', value: 'Device', type: 'text', isSortable: true, },
      { key: 'parameter', value: 'Parameter', type: 'text', isSortable: true, },
      { key: 'condition', value: 'Condition', type: 'text', class: 'capitalize', isSortable: true, },
      { key: 'destination', value: 'Destination', type: 'text', isSortable: true, },
      { key: 'active', value: 'Status', type: 'toggle', headingClass: 'text-center', isSortable: false, },
      { key: '', value: 'Action', type: 'delete-button', headingClass: 'text-center', isSortable: false, }
    ];
  }

  deletedRow(event: any): void {
    const dialogRef = this._fuseConfirmationService.open(this.configForm.value);
    dialogRef.afterClosed().subscribe((result) => { // Subscribe to afterClosed from the dialog reference
      if (result == 'confirmed') {
        this.isLoading = true;
        this._dataService.fetchData({
          apiUrl: apiList.ALERT_GET.replace('{id}', event?._id),
          method: 'DELETE',
          contentType: 'application/json',
          params: undefined,
          body: null
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('Alert removed succesfully.', 'Removed', { duration: 2000 });
          }
        });
        setTimeout(() => { this.eventsSubject.next({ origin: 'deleted' }) }, 1000);
      }
    });
  }

  onRowClicked(event: any): void {
    this.router.navigate(['./', event._id], { relativeTo: this.route });
  }

  selectedRows(rows: []) {
    this.selectedRecords = rows;
  }

  searchString(event: any) {
    this.searchValue = event.search;
  }

  getSelectedSearch(value: string) {
    this.searchValue = value;

    this.eventsSubject.next({ origin: 'users', value: this.searchValue });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  onCheckboxChanged(event: any): void {
    let body = { active: event.checked };
    this._dataService.fetchData({
      apiUrl: apiList.ALERT_GET.replace('{id}', event.item._id),
      method: 'PATCH',
      contentType: 'application/json',
      params: undefined,
      body: body
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this._snackBar.open('Alert status is updated succesfully.', 'Updated', { duration: 2000 });
      } else {
        this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
      }
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
