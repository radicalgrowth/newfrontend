import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlertListComponent } from './alert-list/alert-list.component';
import { AlertFormComponent } from './alert-form/alert-form.component';
import { AlertsResolverService } from './alerts-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: AlertListComponent,
  },
  {
    path: 'add',
    component: AlertFormComponent,
  },
  {
    path: ':id',
    component: AlertFormComponent,
    resolve: {
      data: AlertsResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AlertsRoutingModule { }
