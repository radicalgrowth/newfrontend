import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import * as constantList from 'app/shared/constants/constant-list';
import * as routeList from 'app/shared/constants/routes-list';
import { DataService } from 'app/shared/services/data.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-alert-form',
  templateUrl: './alert-form.component.html',
  styleUrls: ['./alert-form.component.scss']
})

export class AlertFormComponent implements OnInit {

  private _unsubscribeAll: Subject<any> = new Subject<any>();
  user: User;
  isLoading: boolean = false;
  @ViewChild('input') input: ElementRef;
  countries: string[] = constantList.COUNTRIES;
  alertId: any;
  form: FormGroup;
  alert: any;
  public pageIdParam: any = 'id';

  devices: any;
  parameters: any;

  constructor(
    private fb: FormBuilder,
    private _userService: UserService,
    private dataService: DataService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private _dataService: DataService,
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
      this.getDevicesData();
    });

    if (this.isEditMode() != null) {
      this.route?.data.subscribe((data: any) => {
        this.alert = Object.assign({}, data?.data?.body);
        this.alertId = this.alert._id;
        this.form.patchValue({
          active: this.alert.active,
          alert_name: this.alert.alert_name,
          device_id: this.alert.device_id,
          parameter: this.alert.parameter,
          condition: this.alert.condition,
          action: this.alert.action,
          destination: this.alert.destination,
          snooze: this.alert.snooze,
        });
        this.form.updateValueAndValidity();
      });
    }
  }

  isEditMode(param: string = this.pageIdParam): string | null {
    return this.route.snapshot.paramMap.get(param);
  }

  submit() {
    this.isLoading = true;
    let body = this.form.value;
    body.user_id = this.user._id;
    body.destination = this.user.email;
    if (this.isEditMode() != null) {
      body = Object.fromEntries(Object.entries(body).filter(([_, v]) => v != null || v == ''));
    }
    if (this.form.valid) {

      if (this.isEditMode() == null) {
        this._dataService.fetchData({
          apiUrl: apiList.ALERT_BASE_URL,
          method: 'POST',
          contentType: 'application/json',
          params: undefined,
          body: body
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('Alert added succesfully.', 'Added', { duration: 2000 });
            setTimeout(() => { this.router.navigateByUrl(routeList.ALERTS_LISTING) }, 2000);
          } else {
            this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
          }
        });
      } else {
        this._dataService.fetchData({
          apiUrl: apiList.ALERT_GET.replace('{id}', this.alertId),
          method: 'PATCH',
          contentType: 'application/json',
          params: undefined,
          body: body
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('Alert updated succesfully.', 'Updated', { duration: 2000 });
            setTimeout(() => { this.router.navigateByUrl(routeList.ALERTS_LISTING) }, 2000);
          } else {
            this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
          }
        });
      }

    }

  }

  buildForm() {
    this.form = this.fb.group({
      active: [true, [Validators.required]],
      alert_name: [null, [Validators.required, Validators.maxLength(60)]],
      device_id: [null, Validators.required],
      parameter: [null, Validators.required],
      condition: [null, Validators.required],
      action: [null, Validators.required],
      // destination: [null, Validators.required],
      snooze: [null, Validators.required],
    });
  }

  getDevicesData() {
    this._dataService.fetchData({
      apiUrl: apiList.DEVICE_GET_BY_USER.replace('{id}', this.user._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      if (res.status == 'Success') {
        this.devices = res?.body?.items;
        if (this.isEditMode() != null) {
          this.parameters = this.devices.filter((device) => device._id == this.alert.device_id)[0].fields;
        }
      }
    });
  }

  deviceChange() {
    this.parameters = this.devices.filter((device) => device._id == this.form.get('device_id')?.value)[0].fields;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
