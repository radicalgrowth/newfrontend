import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import * as constantList from 'app/shared/constants/constant-list';
import * as routeList from 'app/shared/constants/routes-list';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss']
})

export class ProfileFormComponent implements OnInit {

  loggedInUser: User;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  isLoading: boolean = false;
  image: any = { img: 'assets/images/designer.jpg', file: null };
  @ViewChild('input') input: ElementRef;
  countries: string[] = constantList.COUNTRIES;
  countriesData = constantList.COUNTRIES_DATA;
  maskList = constantList.MASKLIST;
  userId: any;
  form: FormGroup;
  user: any;
  public pageIdParam: any = 'id';
  mask: string = '';

  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private _userService: UserService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private localStorage: LocalStorageService,
  ) {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.loggedInUser = user;
    });
    this.buildForm();
  }

  ngOnInit() {
    this.getUserDetails();
  }

  onImageSelected($event: any) {
    if ($event.target.files.length === 0) return;
    const file = $event.target.files[0];
    this.image.file = file;
    var reader = new FileReader(); // Image upload
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      if (_event) {
        this.image.img = reader?.result?.toString();
      }
    }

  }

  submit() {
    this.form.controls['phone_code'].enable();
    this.isLoading = true;
    let body = this.form.value;
    body = Object.fromEntries(Object.entries(body).filter(([_, v]) => v != null || v == ''));
    if (this.form.valid) {
      var formData: any = new FormData();
      formData.append('photo', this.image?.file);
      formData.append('name', body?.name);
      formData.append('email', body?.email);
      formData.append('phone_number', body?.phone_number);
      formData.append('phone_code', body?.phone_code);
      formData.append('city', body?.city);
      formData.append('country', body?.country);
      formData.append('gender', body?.gender);
      if (body?.password) {
        formData.append('password', body?.password);
      }
      this.dataService.patchFormData(`${apiList.USER_UPDATE}`.replace('{id}', this.userId.toString()), formData).pipe(map((response) => { return response })).subscribe((res) => {
        this.isLoading = false;
        if (res['status'] == 'Success') {
          if (res['body']._id == this.loggedInUser._id) {
            this.localStorage.set({ key: 'user', value: res['body'] });
            location.reload();
          }
          this._snackBar.open('User profile updated succesfully.', 'Updated', { duration: 2000 });
        } else {
          this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
        }
      });
    }

  }

  buildForm() {
    this.form = this.fb.group({
      photo: [null],
      name: [null, [Validators.required, Validators.maxLength(60)]],
      email: [null, [Validators.required, Validators.pattern(constantList.REGEX_EMAIL_ADDRESS)]],
      phone_number: [null, Validators.required],
      phone_code: [null, []],
      country: [null, Validators.required],
      city: [null, Validators.required],
      gender: [null, Validators.required],
      password: [null, Validators.nullValidator],
    });
    this.form.controls['phone_code'].disable();
  }

  getCountryByCode(code: string): any {
    return this.countriesData.find(country => country.code === code);
  }
  getCountryByName(name: string): any {
    return this.countriesData.find(country => country.name === name);
  }
  trackByFn(index: number, item: any): any {
    return item.id || index;
  }

  countrySelected(value) {
    let code = this.getCountryByName(value)?.code;
    this.form.controls['phone_code'].setValue(code);
    this.mask = (this.maskList.find((mask) => mask?.code?.includes(code))?.code).replace(code, '').trim();
  }

  setMask(countryName: string) {
    let code = this.getCountryByName(countryName)?.code;
    this.form.controls['phone_code'].setValue(code);
    this.mask = (this.maskList.find((mask) => mask?.code?.includes(code))?.code).replace(code, '').trim();
  }

  getUserDetails() {
    this.user = Object.assign({}, this.localStorage.get('user'));
    this.userId = this.user._id;
    this.user.photo ? this.image.img = this.user?.photo : this.image.img = 'assets/images/designer.jpg';
    this.form.patchValue({
      name: this.user?.name,
      email: this.user?.email,
      phone_code: this.user?.phone_code,
      phone_number: this.user?.phone_number,
      country: this.user?.country,
      city: this.user?.city,
      gender: this.user?.gender,
    });

    this.setMask(this.user?.country);
    if (this.loggedInUser.role == 'admin') {
    }
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
