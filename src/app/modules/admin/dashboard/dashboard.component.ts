import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CalendarOptions, DateSelectArg, EventApi, EventClickArg } from '@fullcalendar/angular';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import { DataService } from 'app/shared/services/data.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { createEventId, INITIAL_EVENTS } from './events-utils';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {

  user: User;
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  stats: any;
  events: any[] = [];
  currentEvents: EventApi[] = [];
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    initialEvents: INITIAL_EVENTS,
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth'
    },
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    dateClick: this.onDateClick.bind(this),
    events: this.events,
    select: this.addEvent.bind(this),
    eventClick: this.updateDeleteEvent.bind(this),
    eventsSet: this.handleEvents.bind(this),
    dayHeaderFormat: { weekday: 'long' },
    eventTextColor: '#ffffff',
    eventDisplay: 'list-item',
    eventClassNames: function (arg) {
      return arg.event.extendedProps.type == 'irrigation' ? ['irrigation'] : ['custom'];
    }
  };

  isLoading: boolean = false;
  configForm: FormGroup;
  lat: any = 33.6844;
  lng: any = 73.0479;

  weather: any = {
    current: null,
    forecast: null
  };

  constructor(
    private _dataService: DataService,
    private _userService: UserService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _formBuilder: FormBuilder,
    public _matDialog: MatDialog,
    private route: ActivatedRoute,
    private _fuseConfirmationService: FuseConfirmationService,
  ) {
  }

  ngOnInit() {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.user = user;
      this.getStats();
      this.getEvents();
    });
    this.setCurrentPosition();
    this.configForm = this._formBuilder.group({
      title: 'Remove user',
      message: 'Are you sure you want to remove this user permanently? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Remove', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });

  }

  setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.getWeather();
      });
    }
  }

  getStats() {
    this._dataService.fetchData({
      apiUrl: apiList.DASHBOARD_STATS.replace('{id}', this.user._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.stats = res?.body?.items;
      } else {
        this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
      }
    });
  }

  getEvents() {
    this._dataService.fetchData({
      apiUrl: apiList.USER_EVENTS_DETAIL.replace('{id}', this.user._id),
      method: 'GET',
      contentType: 'application/json',
      params: undefined,
      body: null
    }).pipe(map(response => { return response }),
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res.status == 'Success') {
        this.events = res?.body?.items;
        this.calendarOptions.events = this.events;
      } else {
        this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
      }
    });
  }
  getWeather() {
    this.isLoading = true;
    this._dataService.getForkedJoinData(
      apiList.CURRENT_WEATHER.replace('{lat}', this.lat).replace('{lon}', this.lng),
      apiList.WEATHER.replace('{lat}', this.lat).replace('{lon}', this.lng).replace('{exclude}', 'hourly,minutely')
    ).subscribe((res: any) => {
      this.isLoading = false;
      this.weather.current = res[0];
      this.weather.forecast = res[1];
    }, (error: any) => {
      this.isLoading = false;
      console.error(error);
    });
  }


  onDateClick(res) {
    // alert('Clicked on date : ' + res.dateStr);
  }

  addEvent(selectInfo: DateSelectArg) {
    const dialogRef = this._matDialog.open(AddEventDialog, { autoFocus: false, data: { date: selectInfo.startStr } });
    dialogRef.afterClosed().subscribe(res => {
      const calendarApi = selectInfo.view.calendar;
      calendarApi.unselect();
      if (res?.event == 'added') {
        res.data.user_id = this.user._id;
        this._dataService.fetchData({
          apiUrl: apiList.EVENTS_BASE_URL,
          method: 'POST',
          contentType: 'application/json',
          params: undefined,
          body: res.data
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('Event added succesfully.', 'Added', { duration: 2000 });
            this.events.push(res?.body);
            this.events = [...this.events];
            this.calendarOptions.events = this.events;
          } else {
            this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
          }
        });
      }
    });
  }

  updateDeleteEvent(clickInfo: EventClickArg) {
    let eventId;
    if (clickInfo.event.extendedProps._id) {
      eventId = clickInfo.event.extendedProps._id;
    } else {
      eventId = clickInfo.event.id;
    }
    let eventIndex = this.events.findIndex((e) => e._id == eventId);
    if (eventIndex > -1) {
      const dialogRef = this._matDialog.open(AddEventDialog, { autoFocus: true, data: { event: this.events[eventIndex] } });
      dialogRef.afterClosed().subscribe(res => {
        if (res?.event == 'added') {
          res.data.user_id = this.user._id;
          res.data._id = eventId;
          this._dataService.fetchData({
            apiUrl: apiList.EVENTS_DETAIL.replace('{id}', eventId),
            method: 'PATCH',
            contentType: 'application/json',
            params: undefined,
            body: res?.data
          }).pipe(map(response => { return response }),
          ).subscribe((res: any) => {
            this.isLoading = false;
            if (res.status == 'Success') {
              clickInfo.event.remove();
              this.events[eventIndex] = res?.body;
              this.events = [...this.events];
              this.calendarOptions.events = this.events;
              this._snackBar.open('Event updated succesfully.', 'Updated', { duration: 2000 });
            } else {
              this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
            }
          });
        } else if (res?.event == 'deleted') {
          clickInfo.event.remove();
          this.events.splice(eventIndex, 1);
          this.events = [...this.events];
          this.calendarOptions.events = this.events;
          this._dataService.fetchData({
            apiUrl: apiList.EVENTS_DETAIL.replace('{id}', eventId),
            method: 'DELETE',
            contentType: 'application/json',
            params: undefined,
            body: this.events[eventIndex]
          }).pipe(map(response => { return response }),
          ).subscribe((res: any) => {
            this.isLoading = false;
            if (res.status == 'Success') {
              this._snackBar.open('Event deleted succesfully.', 'Deleted', { duration: 2000 });
            } else {
              this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
            }
          });
        }
      });
    }


    // if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
    //   clickInfo.event.remove();
    // }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}




@Component({
  selector: 'add-event-dialog',
  templateUrl: 'add-event-dialog.html',
})
export class AddEventDialog implements OnInit {

  date: any;
  type: string;
  custom_type_text: string;
  value: string;

  editable: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<AddEventDialog>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    if (this._data.event) {
      this.editable = true;
      this.date = new Date(this._data.event.date);
      this.type = this._data.event.type;
      this.custom_type_text = this._data.event.custom_type_text;
      this.value = this._data.event.value;
    } else {
      this.date = new Date(this._data.date);
    }
  }

  save() {
    let body = {};
    if ((this.type == 'irrigation' && this.value && this.date) || (this.type == 'custom' && this.custom_type_text && this.value && this.date)) {
      body = {
        date: new Date(this.date).getFullYear() + '-' + ("0" + (new Date(this.date).getMonth() + 1)).slice(-2) + '-' + ('0' + new Date(this.date).getDate()).slice(-2),
        type: this.type,
        value: this.value,
        custom_type_text: this.custom_type_text || null,
      }
      if (this.type == 'irrigation') {
        body['title'] = (this.type.charAt(0).toUpperCase() + this.type.slice(1)) + ' ' + this.value;
      } else {
        body['title'] = (this.custom_type_text.charAt(0).toUpperCase() + this.custom_type_text.slice(1)) + ' ' + this.value;
      }
      this.dialogRef.close({ event: 'added', data: body });
    } else {
      this._snackBar.open('Fill all the inputs.', 'Failed', { duration: 2000 });
    }
  }

  delete() {
    this.dialogRef.close({ event: 'deleted', data: this._data.event._id });
  }

}