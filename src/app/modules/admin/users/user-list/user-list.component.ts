import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/shared/services/data.service';
import { fromEvent, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith, tap } from 'rxjs/operators';
import * as apiList from 'app/shared/constants/apis-list';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent {

  isLoading: boolean = false;
  configForm: FormGroup;
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  searchControl = new FormControl();
  selectedRecords: any[] = [];
  eventsSubject: Subject<any> = new Subject<any>();
  endPointConfiguration: any = { url: apiList.GET_ALL_USERS, method: 'GET' };
  tableColumns: any;
  requestBody: any;
  searchValue: string;

  @ViewChild('input') input: ElementRef;

  constructor(
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _fuseConfirmationService: FuseConfirmationService,
  ) {
    this.setTableConfigurationByLanguage();
  }

  ngOnInit() {
    this.filteredOptions = this.searchControl.valueChanges.pipe(startWith(''), map((value: string) => this._filter(value)));
    this.configForm = this._formBuilder.group({
      title: 'Remove user',
      message: 'Are you sure you want to remove this user permanently? <span class="font-medium">This action cannot be undone!</span>',
      icon: this._formBuilder.group({
        show: true,
        name: 'heroicons_outline:exclamation',
        color: 'warn'
      }),
      actions: this._formBuilder.group({
        confirm: this._formBuilder.group({ show: true, label: 'Remove', color: 'warn' }),
        cancel: this._formBuilder.group({ show: true, label: 'Cancel' })
      }),
      dismissible: true
    });
  }

  ngAfterViewInit() {

    fromEvent(this.input.nativeElement, 'keyup').pipe(filter(Boolean), debounceTime(500), distinctUntilChanged(), tap(() => {
      this.searchValue = this.input.nativeElement.value;
      this.eventsSubject.next({ origin: 'users', value: this.searchValue });
    })).subscribe();

  }

  setTableConfigurationByLanguage(tran?: any): void {
    this.tableColumns = [
      // { key: 'profile_picture', value: 'Image', type: 'image', class: 'text-left', isSortable: false, },
      { key: 'photo', value: 'Image', type: 'image', class: 'text-left', isSortable: false, },
      { key: 'name', value: 'Name', type: 'text', class: 'capitalize', isSortable: true, },
      { key: 'email', value: 'Email', type: 'text', isSortable: true, },
      { key: 'phone_number', value: 'Contact No', type: 'text', isSortable: true, },
      { key: 'role', value: 'Role', type: 'text', class: 'capitalize', isSortable: true, },
      { key: 'organization', value: 'Organization', type: 'text', class: 'capitalize', isSortable: true, },
      // { key: 'gender', value: 'Gender', type: 'text', class: 'capitalize', isSortable: true, },
      { key: '', value: 'Action', type: 'delete-button', headingClass: 'text-center', isSortable: false, }
    ];
  }

  deletedRow(event: any): void {
    const dialogRef = this._fuseConfirmationService.open(this.configForm.value);
    dialogRef.afterClosed().subscribe((result) => { // Subscribe to afterClosed from the dialog reference
      if (result == 'confirmed') {
        this.isLoading = true;
        this._dataService.fetchData({
          apiUrl: apiList.USER_UPDATE.replace('{id}', event?._id),
          method: 'DELETE',
          contentType: 'application/json',
          params: undefined,
          body: null
        }).pipe(map(response => { return response }),
        ).subscribe((res: any) => {
          this.isLoading = false;
          if (res.status == 'Success') {
            this._snackBar.open('User removed succesfully.', 'Removed', { duration: 2000 });
          }
        });
        setTimeout(() => { this.eventsSubject.next({ origin: 'deleted' }) }, 1000);
      }
    });
  }

  onRowClicked(event: any): void {
    this.router.navigate(['./', event._id], { relativeTo: this.route });
  }

  selectedRows(rows: []) {
    this.selectedRecords = rows;
  }

  searchString(event: any) {
    this.searchValue = event.search;
  }

  getSelectedSearch(value: string) {
    this.searchValue = value;

    this.eventsSubject.next({ origin: 'users', value: this.searchValue });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

}
