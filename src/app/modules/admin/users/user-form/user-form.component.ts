import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';
import * as apiList from 'app/shared/constants/apis-list';
import * as constantList from 'app/shared/constants/constant-list';
import * as routeList from 'app/shared/constants/routes-list';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})

export class UserFormComponent implements OnInit {

  loggedInUser: User;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  isLoading: boolean = false;
  image: any = { img: 'assets/images/designer.jpg', file: null };
  @ViewChild('input') input: ElementRef;
  countries: string[] = constantList.COUNTRIES;
  countriesData = constantList.COUNTRIES_DATA;
  maskList = constantList.MASKLIST;
  userId: any;
  form: FormGroup;
  user: any;
  public pageIdParam: any = 'id';
  mask: string = '';

  constructor(
    private fb: FormBuilder,
    private dataService: DataService,
    private _userService: UserService,
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private _localStorageService: LocalStorageService,
    private router: Router,
  ) {
    this._userService.user$.pipe((takeUntil(this._unsubscribeAll))).subscribe((user: User) => {
      this.loggedInUser = user;
    });
    this.buildForm();
  }

  ngOnInit() {
    if (this.isEditMode() != null) {
      this.route?.data.subscribe((data: any) => {
        this.user = Object.assign({}, data?.data?.body);
        this.userId = this.user._id;
        this.user.photo ? this.image.img = this.user?.photo : this.image.img = 'assets/images/designer.jpg';
        this.setMask(this.user?.country);
        this.form.patchValue({
          name: this.user?.name,
          email: this.user?.email,
          phone_code: this.user?.phone_code,
          phone_number: this.user?.phone_number,
          organization: this.user?.organization,
          country: this.user?.country,
          city: this.user?.city,
          role: this.user?.role,
          gender: this.user?.gender,
          application_name: this.user?.application_name
        });
        this.form.updateValueAndValidity();
        this.form.controls['application_name'].disable();
      });
    } else {
      this.setMask(this.form.value.country);
    }

    if (this.loggedInUser.role == 'admin') {
      this.form.patchValue({
        organization: this.loggedInUser?.organization,
        application_name: this.loggedInUser?.application_name,
        role: 'worker',
      });
      this.form.controls['organization'].disable();
      this.form.controls['role'].disable();
      this.form.controls['application_name'].disable();
    }

  }

  isEditMode(param: string = this.pageIdParam): string | null {
    return this.route.snapshot.paramMap.get(param);
  }

  onImageSelected($event: any) {
    if ($event.target.files.length === 0) return;
    const file = $event.target.files[0];
    this.image.file = file;
    var reader = new FileReader(); // Image upload
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      if (_event) {
        // this.image.file = reader?.result?.toString();
        // this.image.file = _event?.target?.result;
        this.image.img = reader?.result?.toString();
      }
    }

  }

  submit() {
    this.form.controls['phone_code'].enable();
    this.form.controls['application_name'].enable();
    this.form.controls['organization'].enable();
    this.form.controls['role'].enable();
    this.isLoading = true;
    let body = this.form.value;
    if (this.isEditMode() != null) {
      body = Object.fromEntries(Object.entries(body).filter(([_, v]) => v != null || v == ''));
    }
    if (this.form.valid) {
      var formData: any = new FormData();
      formData.append('photo', this.image?.file);
      formData.append('name', body?.name);
      formData.append('email', body?.email);
      formData.append('phone_number', body?.phone_number);
      formData.append('phone_code', body?.phone_code);
      formData.append('city', body?.city);
      formData.append('country', body?.country);
      formData.append('gender', body?.gender);
      formData.append('role', body?.role);
      formData.append('organization', body?.organization);
      formData.append('application_name', body?.application_name);
      if (body?.password) {
        formData.append('password', body?.password);
      }
      if (body?.role == 'worker') {
        formData.append('user_id', this.loggedInUser._id);
      }
      if (this.isEditMode() == null) {
        this.dataService.postFormData(apiList.USER_BASE_URL, formData).pipe(map((response) => { return response })).subscribe((res) => {
          this.isLoading = false;
          if (res['status'] == 'Success') {
            this._snackBar.open('User created succesfully.', 'Created', { duration: 2000 });
            setTimeout(() => { this.router.navigateByUrl(routeList.USERS_LISTING) }, 1000);
          } else {
            this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
          }
        });
      } else {
        this.dataService.patchFormData(`${apiList.USER_UPDATE}`.replace('{id}', this.userId.toString()), formData).pipe(map((response) => { return response })).subscribe((res) => {
          this.isLoading = false;
          if (res['status'] == 'Success') {
            if (res['body']._id == this.loggedInUser._id) {
              this._localStorageService.set({ key: 'user', value: res['body'] });
            }
            this._snackBar.open('User updated succesfully.', 'Updated', { duration: 2000 });
            setTimeout(() => { this.router.navigateByUrl(routeList.USERS_LISTING) }, 1000);
          } else {
            this._snackBar.open('There is some error.', 'Error', { duration: 2000 });
          }
        });
      }

    }

  }

  buildForm() {
    this.form = this.fb.group({
      photo: [null],
      name: [null, [Validators.required, Validators.maxLength(60)]],
      email: [null, [Validators.required, Validators.pattern(constantList.REGEX_EMAIL_ADDRESS)]],
      phone_number: [null, Validators.required],
      phone_code: ['+92', []],
      organization: [null, Validators.required],
      country: ['Pakistan', Validators.required],
      city: [null, Validators.required],
      role: [null, Validators.required],
      gender: [null, Validators.required],
      password: [null, [this.isEditMode() == null ? Validators.required : Validators.nullValidator]],
      application_name: [null, [this.isEditMode() == null ? Validators.required : Validators.nullValidator]],
    });
    this.form.controls['phone_code'].disable();
  }

  getCountryByCode(code: string): any {
    return this.countriesData.find(country => country.code === code);
  }
  getCountryByName(name: string): any {
    return this.countriesData.find(country => country.name === name);
  }
  trackByFn(index: number, item: any): any {
    return item.id || index;
  }

  countrySelected(value) {
    let code = this.getCountryByName(value)?.code;
    this.form.controls['phone_code'].setValue(code);
    this.mask = (this.maskList.find((mask) => mask?.code?.includes(code))?.code).replace(code, '').trim();
  }

  setMask(countryName: string) {
    let code = this.getCountryByName(countryName)?.code;
    this.form.controls['phone_code'].setValue(code);
    this.mask = (this.maskList.find((mask) => mask?.code?.includes(code))?.code).replace(code, '').trim();
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
