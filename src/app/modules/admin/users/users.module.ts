import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from 'app/shared/services/data.service';
import { LocalStorageService } from 'app/shared/services/local-storage.service';
import { SharedModule } from 'app/shared/shared.module';
import { UserFormComponent } from './user-form/user-form.component';
import { UserListComponent } from './user-list/user-list.component';
import { UsersResolverService } from './users-resolver.service';
import { UsersRoutingModule } from './users-routing.module';
import { NgxMaskModule } from 'ngx-mask'

@NgModule({
  declarations: [UserListComponent, UserFormComponent],
  imports: [
    NgxMaskModule.forRoot(),
    ReactiveFormsModule,
    UsersRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [UsersResolverService, DataService, LocalStorageService]
})

export class UsersModule { }
