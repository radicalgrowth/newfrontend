/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/dashboard'
    },
    {
        id: 'users',
        title: 'Users',
        type: 'basic',
        icon: 'heroicons_outline:user-group',
        link: '/users'
    },
    {
        id: 'farms',
        title: 'Farms',
        type: 'basic',
        icon: 'heroicons_outline:template',
        link: '/farms'
    },
    {
        id: 'devices',
        title: 'Devices',
        type: 'basic',
        icon: 'mat_outline:devices',
        link: '/devices'
    },
    {
        id: 'alerts',
        title: 'Alerts',
        type: 'basic',
        icon: 'heroicons_outline:bell',
        link: '/alerts'
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
